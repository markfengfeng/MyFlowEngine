package com.qiangzi.workflow.engine.service;

import org.springframework.core.io.Resource;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;

public interface XmlParserService {

	public ProcessDefinition parse(Resource resource) throws Exception;

}
