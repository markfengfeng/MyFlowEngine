package com.qiangzi.workflow.engine.service;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;

public interface RepositoryService {

	public ProcessInstance deploy(ProcessDefinition topology) throws Exception;

}
