package com.qiangzi.workflow.engine.bpmn.base;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public abstract class Edge extends BaseElement {

	public static final Logger LOGGER = LoggerFactory.getLogger(Edge.class);

	private String sourceRef;
	private String targetRef;

	// 对应的节点
	private Node srcNode;

	private Node targetNode;

	public String getSourceRef() {
		return sourceRef;
	}

	public String getTargetRef() {
		return targetRef;
	}

	public Node getSrcNode() {
		return srcNode;
	}

	public void setSrcNode(Node srcNode) {
		this.srcNode = srcNode;
	}

	public Node getTargetNode() {
		return targetNode;
	}

	public void setTargetNode(Node targetNode) {
		this.targetNode = targetNode;
	}

	public void parse(Element element) throws Exception {

		super.parse(element);

		Assert.isTrue(null != element, "element is null");

		// 提取sourceRef
		Attribute sourceRefAttribute = element.attribute("sourceRef");
		Assert.isTrue(null != sourceRefAttribute, "sourceRef must exist");
		String sourceRef = sourceRefAttribute.getText();
		Assert.hasText(sourceRef, "sourceRef is empty");
		this.sourceRef = sourceRef;

		// 提取targetRef
		Attribute targetRefAttribute = element.attribute("targetRef");
		Assert.isTrue(null != targetRefAttribute, "targetRef must exist");
		String targetRef = targetRefAttribute.getText();
		Assert.hasText(targetRef, "targetRef is empty");
		this.targetRef = targetRef;
	}

	protected void deploy() {
		super.deploy();
	}

	@Override
	public void ignore(ProcessInstance instance) {
		setState(State.IGNORED, instance);
		LOGGER.debug("edge [{}] -> ignored", getId());
	}

}