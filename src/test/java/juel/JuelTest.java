package juel;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import de.odysseus.el.ExpressionFactoryImpl;
import de.odysseus.el.util.SimpleContext;

public class JuelTest {

	public static void main(String[] args) {

		ExpressionFactory factory = new ExpressionFactoryImpl();
		SimpleContext context = new SimpleContext();
		context.setVariable("clarified", factory.createValueExpression("Hello123", String.class));
		//
		ValueExpression e = factory.createValueExpression(context, "${!clarified.equals('yes')}",
				java.lang.Boolean.class);
		System.out.println(e.getValue(context));
		//
		e = factory.createValueExpression(context, "${clarified.length()}", java.lang.Integer.class);
		System.out.println(e.getValue(context));

	}
}
